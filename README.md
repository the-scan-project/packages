To use this repository with Gradle:

```kotlin
repositories {
    maven {
        url = uri("https://gitlab.com/api/v4/projects/39877686/packages/maven")
    }

    mavenCentral()
}
```
